var http = require('http');
var app = require('./config/express');

http
  .createServer(app)
  .listen(5002, function(){
    console.log('Serviço iniciado na porta 5002');
  });