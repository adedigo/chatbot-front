"use strict";

angular.module('BotModule', [])
    .controller('BotCtrl',
        function ($scope, $http, $location, $base64, $rootScope, $uibModal, $window, $q, $timeout, AppConfig, toaster, blockUI) {

            $scope.mensagem = {};

            $scope.conversa = [];

            $scope.enviaMensagem = function (msg) {
                if (!angular.isUndefined(msg) && msg !== "" && msg !== null) {
                    $scope.conversa = $scope.conversa.concat({ "msg": msg });
                    $scope.mensagem.msg = "";
                }
            }

            $scope.limparConversa = function(){
                $scope.conversa = [];
                $scope.mensagem.msg = "";
            }

            $scope.$watch("conversa", function(){
                setTimeout(function() {
                    var elem = document.getElementById('data');
                    elem.scrollTop = elem.scrollHeight;  
                }, 50);
            })

        });