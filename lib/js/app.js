"use strict";

angular.module(
    'AppModule', [
        'ngRoute',
        'base64',
        'ui.bootstrap',
        'BotModule',
        'toaster',
        'ngAnimate',
        'blockUI'
    ])
    .controller('AppController', function () {

    })
    .constant('AppConfig', {
        appInfo: 'http://localhost:5002/appInfo'
    })
    .config(['$routeProvider', '$locationProvider', 'blockUIConfig', function ($routeProvider, $locationProvider, blockUIConfig) {
        blockUIConfig.autoBlock = false;
        blockUIConfig.message = 'Carregando'

        $locationProvider.hashPrefix('');

        $routeProvider.
            when('/', {
                templateUrl: '/views/chatbot.html',
                controller: 'BotCtrl'
            }).
            otherwise({
                redirectTo: '/'
            });

    }])

    ;