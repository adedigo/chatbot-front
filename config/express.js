var express = require('../lib/node_modules/express');
var bodyParser = require('../lib/node_modules/body-parser');

var app = express();

const dotenv = require('../lib/node_modules/dotenv').load();

app.use(express.static('./public/'));
app.use('/external', express.static('./lib/node_modules'));
app.use('/js', express.static('./lib/js'));
app.use('/css', express.static('./lib/css'));
app.use('/components', express.static('./lib/components'));
app.use('/views', express.static('./public/views'));
app.use('/contents', express.static('./lib/contents'));

app.use(bodyParser.json());

app.get('/appInfo', function (req, res) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    res.send({
        "AUTH": process.env.DES_AUTH,
        "URL_API": process.env.DES_URL_API,
        "USERNAME_API": process.env.DES_USERNAME_API,
        "PASSWORD_API": process.env.DES_PASSWORD_API
    });
});

module.exports = app;